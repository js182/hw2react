import React from "react";
import axios from "axios"
import "./App.css";
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";

class App extends React.Component {
  state = {
    products: [],
    cartItems: 0,
  };

  componentDidMount() {
    axios.get('products.json')
      .then((response) => {
        if (Array.isArray(response.data)) {
          this.setState({products: response.data});
          this.loadCartItems();
        } else {
          console.error('Data from products.json is not an array');
        }
      })
      .catch((error) => {
        console.error(`Error loading data from products.json: ${error}`);
      });
  }

  loadCartItems = () => {
    const cart = JSON.parse(localStorage.getItem("cart")) || [];
    this.setState({cartItems: cart.length});
  };

  render() {
    const {products} = this.state;
    return (
      <div className="App">
        <Header/>
        {products.length && <ProductList data={products}/>}
      </div>
    );
  }
}

export default App;
