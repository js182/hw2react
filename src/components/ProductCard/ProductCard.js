import React, {useEffect, useState} from 'react';
import s from "./styles.module.scss"
import Modal from "../Modal/Modal";
import {ReactComponent as StarIcon} from "../../svg/favorites.svg";
import PropTypes from "prop-types";

const ProductCard = ({product}) => {
  const [selected, setSelected] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setSelected(favorites.includes(product.sku));
  }, [product.sku]);

  const handleToggleFavorite = () => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    if (favorites.includes(product.sku)) {
      const newFavorites = favorites.filter(id => id !== product.sku);
      localStorage.setItem('favorites', JSON.stringify(newFavorites));
    } else {
      favorites.push(product.sku);
      localStorage.setItem('favorites', JSON.stringify(favorites));
    }
    setSelected(!selected);
  };

  const handleModalOpen = () => {
    setModalVisible(true);
  };

  const handleModalClose = () => {
    setModalVisible(false);
  };

  const handleConfirm = () => {
    let cart = localStorage.getItem("cart");
    if (cart) {
      cart = JSON.parse(cart);
    } else {
      cart = [];
    }
    cart.push(product);
    localStorage.setItem("cart", JSON.stringify(cart));
    handleModalClose();
  };

  return (
    <div className={s.wrap}>
      <img src={product.image} alt={product.title}/>
      <h2 className={s.title}>{product.title}</h2>
      <p className={s.price}>Price: {product.price}</p>
      <p className={s.sku}> Art: {product.sku}</p>
      <p className={s.color}>{product.color}</p>
      <div className={s.footerCard}>
        <button className={s.cardbutton} onClick={handleModalOpen}> Add to cart</button>
        <StarIcon className={`${s.favoriteStyle} ${selected ? s.selected : ''}`} onClick={handleToggleFavorite}/>
      </div>
      {isModalVisible && (
        <Modal
          header="Confirmation"
          closeButton={true}
          text="Are you sure you want to add this product to your cart?"
          actions={<button onClick={handleConfirm}>Yes</button>}
          onClose={handleModalClose}
        />
      )}
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    sku: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,

}

export default ProductCard;
