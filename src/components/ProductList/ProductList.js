import React from 'react';
import PropTypes from 'prop-types'
import ProductCard from '../ProductCard/ProductCard';
import s from "./productList.module.scss"

const ProductList = ({data}) => {
  return (
    <div className={s.wrap}>
      {data.map((product) => (
        <ProductCard key={product.sku} product={product}/>
      ))}
    </div>
  );
};

ProductList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      sku: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      color: PropTypes.string.isRequired,
    })
  ).isRequired,

};

export default ProductList;



