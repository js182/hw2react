import React, {useEffect, useState} from 'react';
import {ReactComponent as CartIcon} from '../../svg/basket.svg';
import {ReactComponent as StarIcon} from '../../svg/favorites.svg';
import s from "./header.module.scss";

const Header = () => {
  const [cartCount, setCartCount] = useState(0);
  const [favoritesCount, setFavoritesCount] = useState(0);

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartCount(cart.length);

    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavoritesCount(favorites.length);
  }, [localStorage.getItem('cart'), localStorage.getItem('favorites')]);

  return (
    <header className={s.header}>
      <h1 className={s.prodact}>Product list</h1>
      <div className={s.iconsBlock}>
        <div className={s.iconBlock}>
          <CartIcon className={s.icon}/>
          <span className={s.icon}>{cartCount}</span>
        </div>
        <div>
          <StarIcon className={s.icon}/>
          <span className={s.icon}>{favoritesCount}</span>
        </div>
      </div>
    </header>
  );
};

export default Header;